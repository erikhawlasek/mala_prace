# Malá práce

Projekt slouží jako demonstrace použití GitLab pages.

## Použití

Ve složce docs.it4i naleznete markdown strukturu, ze které je následně
díky CI/CD pipeliny vygenerován statický .html content, stačí jej tedy
upravit a poté dojde k automatické kontrole zdrojáků a deployi.

## Instalace

Zpravidla není potřeba, neboť se obsah nachází na GitLabu, pokud by
ale bylo potřeba, lze jej rozběhnout i na localu s použitím vhodných
kontejnerů a závislostí v nich.

## Prerekvizity

GitLab repo, případně možnost nainstalovat python enviroment pro použití
mkdocs generátoru.

## Závislosti

Uvedeny v requirements.txt